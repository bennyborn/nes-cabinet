#include <SPI.h>
#include <Wire.h>
#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

// Pins for driving our CD4021 register
int latchPin = 8;
int dataPin = 9;
int clockPin = 7;

int buttons[8];


void setup() {            

  // init display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();

  // init shift register
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT); 
  pinMode(dataPin, INPUT);
}


void loop() {

  digitalWrite(latchPin,1);
  delayMicroseconds(5);
  digitalWrite(latchPin,0);

  shiftIn(buttons, dataPin, clockPin);

  int bYOffset = 50;
  int r = 7;

  display.clearDisplay();
  
  // A-Button
  if( buttons[7] ) {
    display.drawCircle(120,bYOffset,7,1);
  } else {
    display.fillCircle(120,bYOffset,7,1);
  }
  
  // B-Button
  if( buttons[6] ) {
    display.drawCircle(98,bYOffset,7,1);
  } else {
    display.fillCircle(98,bYOffset,7,1);
  }
  
  // START
  if( buttons[4] ) {
    display.drawRoundRect(68, (bYOffset-1), 15, 5, 2, 1);
  } else {
    display.fillRoundRect(68, (bYOffset-1), 15, 5, 2, 1);
  }
  
  // SELECT
  if( buttons[5] ) {
    display.drawRoundRect(46, (bYOffset-1), 15, 5, 2, 1);
  } else {
    display.fillRoundRect(46, (bYOffset-1), 15, 5, 2, 1);
  }

  // D-PAD: LEFT
  if( buttons[1] ) {
    display.drawRoundRect(0, 26, 15, 15, 0, 1);
  } else {
    display.fillRoundRect(0, 26, 15, 15, 0, 1);
  }
  
  // D-PAD: RIGHT
  if( buttons[0] ) {
    display.drawRoundRect(30, 26, 15, 15, 0, 1);
  } else {
    display.fillRoundRect(30, 26, 15, 15, 0, 1);
  }
  
  // D-PAD: UP
  if( buttons[3] ) {
    display.drawRoundRect(15, 11, 15, 15, 0, 1);
  } else {
    display.fillRoundRect(15, 11, 15, 15, 0, 1);
  }
  
  // D-PAD: DOWN
  if( buttons[2] ) {
    display.drawRoundRect(15, 40, 15, 15, 0, 1);
  } else {
    display.fillRoundRect(15, 40, 15, 15, 0, 1);
  }
  
  display.display();
}

void shiftIn(int *buttons, int data, int clock) {

  int i;
  int b;

  pinMode(clock, OUTPUT);
  pinMode(data, INPUT);

  for( i=7; i>=0; i-- )
  {
    digitalWrite(clock, 0);
    delayMicroseconds(0.2);
    
    buttons[i] = digitalRead(data) ? 1 : 0;
    
    digitalWrite(clock, 1);
  }
}
