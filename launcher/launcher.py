#!/usr/bin/python
# -*- coding: UTF-8 -*-
import pygame
import sys
import os
import math
import time
import subprocess
from pygame.locals import *


def main():

	pygame.init()

	# add joystick support
	pygame.joystick.init()
	joystick = pygame.joystick.Joystick(0)
	joystick.init()

	# disable mouse support
	pygame.event.set_blocked(pygame.MOUSEMOTION)

	# set buttons
	if sys.platform == "win32":
		BTN_START = 7
	else:
		BTN_START = 3

	BTN_START_TBEGIN = 0

	clock = pygame.time.Clock()

	while True:

		clock.tick(60)

		# check for events
		for event in pygame.event.get():

			if event.type == pygame.JOYBUTTONDOWN:

				# START Button
				if event.button == BTN_START:
					BTN_START_TBEGIN = time.time()

			elif event.type == pygame.JOYBUTTONUP:

				# START Button
				if event.button == BTN_START:
					duration = time.time() - BTN_START_TBEGIN

					if duration >= 10:

						if sys.platform == "win32":
							pass
						else:
							os.system("sudo halt")

						print "HALT SYSTEM"

					elif duration >= 5:

						if sys.platform == "win32":
							
							subprocess.call(["python", path+"/menu.py"])

						else:
							os.system("sudo pkill -9 -e fceux")
							os.system("sudo pkill -9 -e -f menu.py")
							path = os.path.abspath(os.path.dirname(__file__))
							p = subprocess.Popen(["/usr/bin/python", path+"/menu.py"])

						print "RETURN TO MENU"

					else:
						pass

					sys.stdout.flush()


if __name__ == '__main__':
	path = os.path.abspath(os.path.dirname(__file__))
	p = subprocess.Popen(["/usr/bin/python",path+"/menu.py"])
	main()