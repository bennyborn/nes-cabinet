#!/usr/bin/python
# -*- coding: UTF-8 -*-
import pygame
import pygame.mixer
import pygame.gfxdraw
import sys
import os
import math
import time
import subprocess
from pygame.locals import *


class Menu:

	def __init__(self,directory="./roms"):

		self.items = []
		self.selected = 0

		# get list of roms
		files = os.listdir(directory)
		files.sort()

		for file in files:
			if os.path.isfile(directory+"/"+file):
				self.items.append({ 'path':os.path.abspath(directory+"/"+file), 'name':os.path.splitext(file)[0], 'active':False })

		self.__selectItem(0)

	def __selectItem(self,index):

		if index < 0:
			index = 0
		if index > (len(self.items)-1):
			index = (len(self.items)-1)

		# de-select previous entry
		self.items[self.selected]['active'] = False

		# select new entry
		self.items[index]['active'] = True		
		self.selected = index

	def selectNext(self):
		self.__selectItem( self.selected+1 )

	def selectPrev(self):
		self.__selectItem( self.selected-1 )

	def getItems(self,limit=5):
		offset = (int(math.floor(self.selected/limit))*limit)
		return self.items[offset:(offset+limit)]

	def getSelected(self):
		return self.items[ self.selected ]


def aspect_scale(img,(bx,by)):

    ix,iy = img.get_size()
    if ix > iy:
        # fit to width
        scale_factor = bx/float(ix)
        sy = scale_factor * iy
        if sy > by:
            scale_factor = by/float(iy)
            sx = scale_factor * float(ix)
            sy = by
        else:
            sx = bx
    else:
        # fit to height
        scale_factor = by/float(iy)
        sx = scale_factor * ix
        if sx > bx:
            scale_factor = bx/float(ix)
            sx = bx
            sy = scale_factor * float(iy)
        else:
            sy = by

    return pygame.transform.scale(img, (int(sx),int(sy)))


def blitScaled( screen, main ):

	# scale main surface preserving aspect ratio
	scaled = aspect_scale(main, (screen.get_size()))

	# position scaled surface in the middle of the screen
	rect = scaled.get_rect()
	rect.centerx = screen.get_rect().centerx
	rect.centery = screen.get_rect().centery

	# blit scaled main surface to screen
	screen.blit(scaled, rect )
	pygame.display.flip()


def runSelected( item,screen,main ):

	sound = pygame.mixer.Sound('select.wav')
	sound.play()

	clock = pygame.time.Clock()

	# draw lens-fade (like in super mario)
	for r in range (1,main.get_height(),13):
		pygame.gfxdraw.filled_ellipse(main, main.get_rect().centerx, main.get_rect().centery, r, int(r/1.3), (0,0,0) )
		blitScaled(screen, main)
		clock.tick(22)

	if sys.platform == "win32":

		subprocess.call(["./fceux.exe",item['path']])

	else:

		path = os.path.abspath(os.path.dirname(__file__))
		p = subprocess.Popen([path+"/fceux",item['path']])
		pygame.quit()


def main():

	pygame.mixer.init(buffer=64)
	pygame.init()

	# add joystick support
	pygame.joystick.init()
	joystick = pygame.joystick.Joystick(0)
	joystick.init()

	# disable mouse support
	pygame.event.set_blocked(pygame.MOUSEMOTION)

	# main surface for our launcher, we use this to be able to scale everything independently
	main = pygame.Surface((256, 224))

	pygame.display.set_caption("NES Launcher")

	# init screen (platform dependent)
	if sys.platform == "win32":
		screen = pygame.display.set_mode((main.get_width()*2, main.get_height()*2),HWSURFACE|DOUBLEBUF|RESIZABLE)
	else:
		screen = pygame.display.set_mode()
	
	icon = pygame.image.load("icon.png").convert_alpha()
	pygame.display.set_icon(icon)

	pygame.mouse.set_visible(0)
	pygame.key.set_repeat(1, 30)
	clock = pygame.time.Clock()

	# animate lava at the bottom
	ANIMATELAVA = USEREVENT + 1
	lava_x = 0
	lava = pygame.image.load('lava.png')
	pygame.time.set_timer(ANIMATELAVA, 500)

	menu = Menu()
	font = pygame.font.Font("./menu.ttf",8)

	while True:

		clock.tick(15)

		# check for events
		for event in pygame.event.get():

			# quit
			if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE) or (event.type == pygame.JOYBUTTONDOWN and event.button == 2):
				pygame.quit()
				return

			# menu navigation (keyboard)
			elif event.type == pygame.KEYDOWN:

				if event.key == pygame.K_RETURN:
					runSelected( menu.getSelected(),screen,main )
				elif event.key == pygame.K_UP:
					menu.selectPrev()
				elif event.key == pygame.K_DOWN:
					menu.selectNext()

			# menu navigation (joystick)
			elif event.type == pygame.JOYAXISMOTION or event.type == pygame.JOYBUTTONDOWN:

				# digi-cross
				if event.type == pygame.JOYAXISMOTION:
					if event.dict['value'] <= -1:
						menu.selectPrev()
					elif event.dict['value'] >= 1:
						menu.selectNext()

				# start button
				elif event.type == pygame.JOYBUTTONDOWN and event.button == 3:
					runSelected( menu.getSelected(),screen,main )

			# animate lava (set position)
			elif event.type == ANIMATELAVA:
				lava_x +=2
				if lava_x > 12:
					lava_x = 0

			# resizing
			elif event.type == VIDEORESIZE:
				screen = pygame.display.set_mode(event.dict['size'],HWSURFACE|DOUBLEBUF|RESIZABLE)
				pygame.display.flip()

		# fill with background color
		main.fill((0, 0, 0))

		# draw menu
		xPos = 80
		for item in menu.getItems( 5 ):

			if item['active']:
				pygame.draw.rect(main, (245, 25, 18), (0,xPos-5,main.get_width(),8), 8)

			menuItem = font.render(item['name'], 0, (255,255,255))

			textrect = menuItem.get_rect()
			textrect.centerx = main.get_rect().centerx
			textrect.centery = xPos

			main.blit(menuItem, textrect)

			xPos = xPos+20

		main.blit(lava, (lava_x,210))

		# draw foreground image
		foreground = pygame.image.load("foreground.png")
		main.blit(foreground, (0, 0))

		# scale and blit to full screen size
		blitScaled(screen, main)

if __name__ == '__main__':
	main()
